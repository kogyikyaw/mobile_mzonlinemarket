import 'package:flutter/material.dart';
import 'package:flutter_webview_project/utils/all_translations.dart';

class InternetNotAvailable extends StatelessWidget {
  const InternetNotAvailable({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      color: Colors.grey,
      padding: EdgeInsets.all(16.0),
      child: Text(allTranslations.text("pls_check_connection"),
          style: TextStyle(color: Colors.white, fontSize: 18)),
    );
  }
}
