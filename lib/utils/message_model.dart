import 'package:meta/meta.dart';
import 'dart:convert';

class FirebaseMessageModel {
  final String uuid;
  final String title;
  final String description;
  final int is_read;

  FirebaseMessageModel({@required this.uuid, @required this.title, @required this.description, @required this.is_read});

  factory FirebaseMessageModel.fromRawJson(String str) => FirebaseMessageModel._fromJson(jsonDecode(str));

  String toRawJson() => jsonEncode(_toJson());

  factory FirebaseMessageModel._fromJson(Map<String, dynamic> json) => FirebaseMessageModel(
    uuid: json['uuid'],
    title: json['title'],
    description: json['description'],
    is_read: json['is_read'],
  );


  Map<String, dynamic> _toJson() => {
    'uuid':uuid,
    'title': title,
    'description': description,
    'is_read': is_read,
  };

  Map<String, dynamic> toMap() {
    return {'uuid': uuid, 'title': title,  'description': description, 'is_read' : is_read};
  }


}