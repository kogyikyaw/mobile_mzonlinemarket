import 'package:flutter/foundation.dart';

class CountNotifier with ChangeNotifier {

  int cnt;

  int get count => cnt;

  void setCount(int count) {
    cnt = count;
    notifyListeners();
  }
}