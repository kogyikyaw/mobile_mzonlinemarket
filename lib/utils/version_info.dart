import 'dart:convert';

import 'package:meta/meta.dart';


class VersionInfo{

   final String version;

   final List<String> deprecated_versions;


   VersionInfo({@required this.version, @required this.deprecated_versions});

   factory VersionInfo.fromRawJson(String str) => VersionInfo._fromJson(jsonDecode(str));


   factory VersionInfo._fromJson(Map<String, dynamic> json) => VersionInfo(
     version: json['version'],
     deprecated_versions: json['deprecated_versions'] != null
         ? List<String>.from(json['deprecated_versions'])
         : null,
   );



}
