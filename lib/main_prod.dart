import 'package:flutter_webview_project/app.dart';
import 'package:flutter_webview_project/utils/all_translations.dart';

import 'app_config.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;
import 'package:flutter/foundation.dart';

void main() async {

  // Initializes the translation module
  WidgetsFlutterBinding.ensureInitialized();
  await allTranslations.init();

  var configuredApp = new AppConfig(
    websiteName: "MZ Online Market",
    flavorName: 'Production',
    isDebugMode: (!kReleaseMode && !kProfileMode),
    osName: Platform.isAndroid? "Android" : "iOS",
    websiteUrl: 'https://mzonlinemarket.com',
    child: MyApp(),
  );

  runApp(configuredApp);
}
