import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_project/utils/all_translations.dart';
import 'package:flutter_webview_project/utils/count_notifier.dart';
import 'package:flutter_webview_project/utils/sqlite_db_helper.dart';
import 'package:provider/provider.dart';
import 'inbox_detail_page.dart';
import 'utils/message_model.dart';

class InboxPage extends StatefulWidget {

  @override
  _InboxPageState createState() {
    return new _InboxPageState();
  }
}

class _InboxPageState extends State<StatefulWidget>{
  Color titleColor = Color(0xfff5f5f5);

  List<FirebaseMessageModel> messageList = new List();
  var notifier;

  @override
  Widget build(BuildContext context) {
    notifier = Provider.of<CountNotifier>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          title:Text(allTranslations.text("inbox"), style: TextStyle(fontSize: 16,)),
          actions: [
            GestureDetector(
              child: Visibility(
                visible: messageList.length>10,
                child: Padding(
                  padding: EdgeInsets.only(right: 8),
                  child: Icon(Icons.delete, color: Colors.black,),
                ),

              ),
              onTap: () {
                _showDeleteAllConfirmationDialog(context);
              },
            ),
          ],
      ),
      body: Container(
        //margin: EdgeInsets.only(top: 10),
        child: Center(
          child: Stack(children: [
            Visibility(
              visible: messageList.length>0,
              child: ListView.builder(
                  itemCount: messageList.length,
                  itemBuilder: (BuildContext context,int index) {
                    return Dismissible(
                        background: stackBehindDismiss(),
                        key: ObjectKey(messageList[index]),
                        child: Container(
                            color: messageList[index].is_read == 1 ? null : titleColor,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                ListTile(
                                  title: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      RawMaterialButton(
                                        onPressed: () {},
                                        elevation: 0.0,
                                        fillColor: Colors.yellow,
                                        child: Icon(Icons.add_shopping_cart,
                                          color: Colors.white,),
                                        padding: EdgeInsets.all(2.0),
                                        shape: CircleBorder(),
                                        constraints: BoxConstraints.expand(
                                          width: 36, height: 36,),
                                      ),


                                      SizedBox(
                                        width: 5, // here put the desired space between the icon and the text
                                      ),
                                      Expanded(child:
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceEvenly,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(top: 8),
                                            child:Text(messageList[index].title,
                                              style: TextStyle(
                                                  fontSize: 14.0, color: Colors.black),) ,
                                          ),

                                          Padding(
                                            padding: EdgeInsets.only(top: 8.0),
                                            child: Text(messageList[index].description,
                                                style: TextStyle(fontSize: 13.0,
                                                    color: Colors.grey)),
                                          ),
                                        ],)
                                      )
                                      // here we could use a column widget if we want to add a subtitle
                                    ],
                                  ),
                                  onTap: () =>
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) =>
                                            InboxDetailPage(
                                              data: messageList[index],
                                            )),
                                      ),
                                ),

                                //Divider(height: 5, thickness: 2, color: titleColor,),

                              ],
                            )
                        ),
                        onDismissed: (direction) {
                          var message = messageList.elementAt(index);

                          // Show a snackbar. This snackbar could also contain "Undo" actions.
                         final scaff = Scaffold.of(context);
                             scaff.showSnackBar(SnackBar(
                                              backgroundColor: Colors.black,
                                              content: Text(allTranslations.text("message_deleted_successfully"), style: TextStyle(color: Colors.white)),
                                              duration: Duration(seconds: 5),

                          )
                          );
                          // Remove the item from the data source.
                          deleteItem(index, message, direction);
                        },
                        confirmDismiss: (DismissDirection dismissDirection) async {
                          switch(dismissDirection) {
                            case DismissDirection.endToStart:
                              return await _showConfirmationDialog(context) == true;
                            case DismissDirection.startToEnd:
                              return await _showConfirmationDialog(context) == true;
                            case DismissDirection.horizontal:
                            case DismissDirection.vertical:
                            case DismissDirection.up:
                            case DismissDirection.down:
                              assert(false);
                          }
                          return false;
                        },

                    );
                  }
              ),
            ),
            Visibility(
              visible: messageList.length ==0 ,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.message, color: Colors.black,),
                  Padding(
                      padding: EdgeInsets.only(top: 7),
                      child: Text(allTranslations.text("any_message_received"), style: TextStyle(color: Colors.black45),)),

                ],
              ),

            ),
          ],),
        )
        ,
      )
            );

  }

  @override
  void initState() {
    getAllMessageList();
  }

  void getAllMessageList(){
    DatabaseHelper.instance.queryAllRows().then((value) {
      setState(() {
        value.forEach((element) {
          messageList.add(FirebaseMessageModel(uuid: element['uuid'], title: element["title"], description: element["description"],
              is_read: element['is_read']));
        });
      });
    }).catchError((error) {
      print(error);
    });
  }

  void deleteItem(index, message, direction){
    setState((){
      messageList.removeAt(index);
      DatabaseHelper.instance.delete(message);
      DatabaseHelper.instance.getUnreadCount().then((value) {
        setState(() {
          print("count: $value");
          notifier.setCount(value);
        });
      }).catchError((error) {
        print(error);
      });

    });
  }

  void undoDeletion(index, item){
    setState((){
      messageList.insert(index, item);
    });
  }

  void clearAllMessages(){
    setState((){
      DatabaseHelper.instance.clearTable();
      messageList.clear();
      DatabaseHelper.instance.getUnreadCount().then((value) {
        setState(() {
          print("count: $value");
          notifier.setCount(value);
        });
      }).catchError((error) {
        print(error);
      });
    });
  }

  Widget stackBehindDismiss() {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(right: 20.0),
      color: Colors.orange,
      child: Icon(
        Icons.delete,
        color: Colors.white,
      ),
    );
  }

  Future<bool> _showConfirmationDialog(BuildContext context) {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          title: Text(allTranslations.text("want_delete_message"), style: TextStyle(color: Colors.black),),
          actions: <Widget>[
            FlatButton(
              child: Text(allTranslations.text('confirm_to_delete'), style: TextStyle(color: Colors.black),),
              onPressed: () {
                Navigator.pop(context, true); // showDialog() returns true
              },
            ),
            FlatButton(
              child: Text(allTranslations.text('not_confirm_to_delete'), style: TextStyle(color: Colors.black),),
              onPressed: () {
                Navigator.pop(context, false); // showDialog() returns false
              },
            ),
          ],
        );
      },
    );
  }

  Future<bool> _showDeleteAllConfirmationDialog(BuildContext context) {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          title: Text(allTranslations.text("sure_to_delete_all" ), style: TextStyle(color: Colors.black),),
          actions: <Widget>[
            FlatButton(
              child: Text(allTranslations.text('confirm_to_delete'), style: TextStyle(color: Colors.black),),
              onPressed: () {
                  Navigator.pop(context, true);
                  clearAllMessages();
              },
            ),
            FlatButton(
              child: Text(allTranslations.text('not_confirm_to_delete'), style: TextStyle(color: Colors.black),),
              onPressed: () {
                Navigator.pop(context, false); // showDialog() returns false
              },
            ),
          ],
        );
      },
    );
  }

}
