
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_project/utils/all_translations.dart';
import 'package:flutter_webview_project/utils/count_notifier.dart';
import 'package:flutter_webview_project/utils/message_model.dart';
import 'package:flutter_webview_project/utils/sqlite_db_helper.dart';
import 'package:provider/provider.dart';

import 'app_config.dart';


class InboxDetailPage extends StatefulWidget {

  final FirebaseMessageModel data;

  InboxDetailPage({Key key, this.data}) : super(key: key);

  @override
  _InboxDetailPage createState() {
    return new _InboxDetailPage(data);
  }

}

class _InboxDetailPage extends State<StatefulWidget>{

  bool validate = false;
  var appConfig;
  var notifier;
  FirebaseMessageModel message;
  Color bgColor = Color(0xfff5f5f5);

  _InboxDetailPage(this.message);

  @override
  void dispose() {
    super.dispose();
  }


  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    notifier = Provider.of<CountNotifier>(context);
    appConfig = AppConfig.of(context);
    DatabaseHelper.instance.updateIsRead(message).then((value) {
      setState(() {
        print("db update: $value");
      });
    }).catchError((error) {
      print(error);
    });

    return Scaffold(
      backgroundColor: Colors.white,
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                leading: new IconButton(
                  icon: new Icon(Icons.arrow_back),
                  onPressed: () {
                    DatabaseHelper.instance.getUnreadCount().then((value) {
                      setState(() {
                        print("count: $value");
                        notifier.setCount(value);
                      });
                    }).catchError((error) {
                      print(error);
                    });
                    Navigator.pop(context);
                  },
                ),
                floating: false,
                pinned: true,
                title: Text(allTranslations.text("detail"), style: TextStyle(fontSize: 16,)),
                backgroundColor: bgColor,
              ),

            ];
          },
          body: SingleChildScrollView(
              child: Container(
              //margin: EdgeInsets.only(top: 120),
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  //alignment: Alignment.center,
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        ListTile(
                          title: Text(message.title, style: TextStyle(fontSize: 14.0, color: Colors.black),),
                          subtitle: Padding(
                            padding: EdgeInsets.only(top: 10),
                            child: Text(message.description, style: TextStyle(fontSize: 13.0, color: Colors.grey)),),

                          onTap: (){

                          }
                        ),

                      ],
                    ),

                ],),

              )
          ),

          ),
        );
  }


  _pushLoginAndRegistrationPage(){
    Navigator.of(context).pushNamedAndRemoveUntil(
        '/loginregistrationpage', (Route<dynamic> route) => false
    );
  }


}
