import 'dart:async';
import 'dart:io';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_project/utils/all_translations.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'app_config.dart';
import 'internet_not_available.dart';

class WebViewFlutter extends StatefulWidget {

  final String websiteName;
  final String websiteUrl;
  final String useragent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36";

  const WebViewFlutter({Key key, this.websiteName, this.websiteUrl})
      : super(key: key);

  @override
  _WebViewFlutterState createState() => _WebViewFlutterState();
}

class _WebViewFlutterState extends State<WebViewFlutter> {

  final Completer<WebViewController> _controller =  Completer<WebViewController>();
  bool isLoading=true;
  final _key = UniqueKey();
  WebViewController webView;
  final Color svgColor = Color(0xffed1a3b);
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
  FirebaseAnalyticsObserver(analytics: analytics);
  var appConfig;
  String language = allTranslations.currentLanguage;


  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    //SystemChannels.textInput.invokeMethod('TextInput.hide');
    appConfig = AppConfig.of(context);
    return WillPopScope(
        onWillPop: _onBack,
        child: Scaffold(
          body: Container(
            color: Colors.white,
            child : Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                /*Visibility(
                    visible: Provider.of<DataConnectionStatus>(context) ==
                        DataConnectionStatus.disconnected,
                    child: InternetNotAvailable()),*/
                Expanded(
                    child: Center(
                      child: Stack(
                        children: [
                          WebView(
                            initialMediaPlaybackPolicy: AutoMediaPlaybackPolicy.always_allow,
                            key: _key,
                            initialUrl: widget.websiteUrl,
                            javascriptMode: JavascriptMode.unrestricted,
                            userAgent: WebSocket.userAgent + "mz_mobile_app",
                            gestureNavigationEnabled: true,
                            onWebViewCreated: (WebViewController webViewController) {
                              webView = webViewController;
                              _controller.complete(webView);
                            },
                            onPageStarted: (String url) {
                              print('onPageStarted: $url');
                            },
                            onPageFinished: (String url) {
                              print('onPageFinished: $url');
                              setState(() {
                                isLoading = false;
                                if(url.contains("sociallogin/account/login/type/google/")) { //for google login redirect page not working
                                  print("redirect url " + url);
                                  isLoading = true;
                                  webView.loadUrl(appConfig.websiteUrl);
                                }else {
                                  isLoading = false;
                                }

                              });

                            },
                            navigationDelegate: (action){
                              print("in navigation Delegate "+ action.url);
                              _sendCurrentScreen(action.url);
                              _sendAnalyticsEvent(action.url);
                              return NavigationDecision.navigate;
                            },


                            debuggingEnabled: true,

                          ),
                          isLoading ? Container(
                                         color: Colors.white,
                                         child: Center(
                                              child: CircularProgressIndicator(
                                                backgroundColor: Colors.orange,
                                                valueColor: new AlwaysStoppedAnimation<Color>(svgColor),
                                                strokeWidth: 3,)

                                         ),
                          )  : Stack(),
                        ],
                      ),
                    ))
              ],
            ), //
          ),

        ));
  }

  Future<bool> _onBack() async {
    bool goBack = false;
    WebViewController webViewController = await _controller.future;
    bool canNavigate = await webViewController.canGoBack();
    if (canNavigate) {
      webViewController.goBack();
      return false;
    } else {
      await showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          backgroundColor: Colors.white,
          title: new Text(allTranslations.text('confirmation'), style: TextStyle(color: Colors.black)),
          content: new Text(allTranslations.text('want_to_exit'), style: TextStyle(color: Colors.black)),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop(false);
                setState(() {
                  goBack = false;
                });
              },
              child: new Text(allTranslations.text("no"), style: TextStyle(color: Colors.black)), // No
            ),

            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  goBack = true;
                });
              },
              child: new Text(allTranslations.text("yes"), style: TextStyle(color: Colors.black)), // Yes
            ),
          ],
        ),
      );
      if (goBack)
        //Navigator.popUntil(context, ModalRoute.withName('/'),);
        exit(0);
      return goBack;
    }
  }

  Future<void> _sendCurrentScreen(String screenName) async {
    await analytics.setCurrentScreen(
      screenName: screenName,
      screenClassOverride: screenName,
    );

  }

  Future<void> _sendAnalyticsEvent(String url) async {
    await analytics.logEvent(
      name: 'link event',
      parameters: <String, dynamic>{
        'url': url,

      },
    );

  }


  }



