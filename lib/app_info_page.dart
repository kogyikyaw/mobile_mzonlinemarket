
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_project/utils/all_translations.dart';
import 'package:flutter_webview_project/utils/count_notifier.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'app_config.dart';


class AppInfoPage extends StatefulWidget {
  final Map<String, dynamic> data;
  AppInfoPage({Key key, this.data}) : super(key: key);

  @override
  _AppInfoPageState createState() {
    return new _AppInfoPageState();
  }

}

class _AppInfoPageState extends State<StatefulWidget>{
  bool validate = false;
  var appConfig;
  var notifier;
  String language = allTranslations.currentLanguage;
  Color bgColor = Color(0xfff5f5f5);

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const String toLaunch = 'https://mzonlinemarket.com/about-us';
    appConfig = AppConfig.of(context);
    Future<void> _launched;
    notifier = Provider.of<CountNotifier>(context);
    String languageText = "";
    if(language == 'en'){
      languageText = 'Change to Myanmar';
    }else{
      languageText = 'Change to English';
    }

    return Scaffold(
      backgroundColor: Colors.white,
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                floating: false,
                pinned: true,
                title: Text(allTranslations.text('info'), style: TextStyle(fontSize: 16,)),
              ),
            ];
          },
          body: SingleChildScrollView(
              child: Container(
              //margin: EdgeInsets.only(top: 120),
                margin: EdgeInsets.only(top: 10),
                child: Column(
                  //alignment: Alignment.center,
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        ListTile(
                          leading: const Icon(Icons.call,  color: Colors.grey),
                          title: Text(allTranslations.text("call_us"), style: TextStyle(color: Colors.black87),),
                          onTap: (){
                            /*Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => TransactionHistoryPage()
                            )
                            );*/
                            launch("tel://+959 779 900 888");
                          }
                        ),

                        /*Divider(height: 5, thickness: 2, color: Colors.black12,),

                        ListTile(
                          leading: const Icon(Icons.help,  color: Colors.grey),
                          title: Text(allTranslations.text("about_us"), style: TextStyle(color: Colors.black87),),
                            onTap: (){
                              setState(() {
                                _launched = _launchInBrowser(toLaunch);
                              });
                            }
                        ),*/

                        Divider(height: 5, thickness: 2, color: Colors.black12),

                        ListTile(
                          leading: const Icon(Icons.language, color: Colors.grey,),
                          trailing: Image.asset(language == 'en'? "assets/images/language.png" : "assets/images/myanmar_lan.png",  width: 20, height: 20,),
                          title:
                          Text(allTranslations.text("language"), style: TextStyle(color: Colors.black87),),

                          onTap: ()async{
                            if(language == 'my'){
                              language = 'en';
                            }else{
                              language = 'my';
                            }
                            await allTranslations.setNewLanguage(language, true);
                            setState((){});
                          },
                        ),

                        Divider(height: 5, thickness: 2, color: Colors.black12),

                        ListTile(
                          leading: const Icon(Icons.info, color: Colors.grey,),
                          title: Container(
                            //color: Colors.red,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(allTranslations.text("version"), style: TextStyle(color: Colors.black87),),
                                FutureBuilder(
                                  future: _getVersionNumber(),
                                  builder: (BuildContext context, AsyncSnapshot<String> snapshot) =>
                                      Text(
                                        snapshot.hasData ? snapshot.data : "Loading ...",
                                        style: TextStyle(fontSize: 13, fontFamily: "InterSemiBold", color: Colors.black45),
                                      ),
                                ),

                              ],
                            ),


                          ),
                        ),

                        Divider(height: 5, thickness: 2, color: Colors.black12),
                      ],
                    ),

                ],),

              )
          ),

          ),
        );
  }

  Future<bool> _showExitDialog() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(false),
            child: Text("NO"),
          ),
          SizedBox(height: 16),
          new GestureDetector(
            onTap: () => _pushLoginAndRegistrationPage(),
            child: Text("YES"),
          ),
        ],
      ),
    ) ??
        false;
  }

  _pushLoginAndRegistrationPage(){
    Navigator.of(context).pushNamedAndRemoveUntil(
        '/loginregistrationpage', (Route<dynamic> route) => false
    );
  }

  Future<String> _getVersionNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = packageInfo.version + "(" + (appConfig.isDebugMode? "debug" : "release") + ")";

    return version;
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }

}
