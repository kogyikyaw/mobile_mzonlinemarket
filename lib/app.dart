import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_webview_project/app_info_page.dart';
import 'package:flutter_webview_project/services/data_connectivity_service.dart';
import 'package:flutter_webview_project/utils/all_translations.dart';
import 'package:flutter_webview_project/utils/count_notifier.dart';
import 'package:flutter_webview_project/webview_page.dart';
import 'package:splashscreen/splashscreen.dart';
import 'inbox_page.dart';
import 'home_page.dart';
import 'package:provider/provider.dart';

class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() {
    return new _MyAppState();
  }
}

class _MyAppState extends State<StatefulWidget> {
  final Color svgColor = Color(0xffed1a3b);

  @override
  void initState(){
    super.initState();

    // Initializes a callback should something need
    // to be done when the language is changed
    allTranslations.onLocaleChangedCallback = _onLocaleChanged;

  }

  ///
  /// If there is anything special to do when the user changes the language
  ///
  _onLocaleChanged() async {
    // do anything you need to do if the language changes
    print('Language has been changed to: ${allTranslations.currentLanguage}');
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    Color titleColor = Color(0xfff5f5f5);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return
      MultiProvider(
        providers: [
          ChangeNotifierProvider<CountNotifier>(create: (context) => CountNotifier()),
          StreamProvider<DataConnectionStatus>(
          create: (context) {
              return DataConnectivityService()
                  .connectivityStreamController
                  .stream;
              },
          ),
        ],
        child:  MaterialApp(

          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],

          supportedLocales: allTranslations.supportedLocales(),
          theme: ThemeData(    // Define the default brightness and colors.
            brightness: Brightness.dark,
            primaryColor: titleColor,
            accentColor: titleColor,
          ),
          //theme: ThemeData(primarySwatch: Colors.deepOrange),
          routes: {
            '/': (BuildContext context) =>
                  Stack(
                    children: <Widget>[
                    SplashScreen(
                    seconds: 2,
                    navigateAfterSeconds:  MyHomePage(),
                    backgroundColor: Colors.white,
                    loaderColor: Colors.transparent,
                    pageRoute: _createRoute(),
                    ),
                    Container(
                    decoration: BoxDecoration(
                    image: DecorationImage(
                    image: AssetImage("assets/images/mz.gif"),
                    fit: BoxFit.fitWidth,
                    ),
                    ),
                    ),
                    ],

                  ),
            'info':(BuildContext context) => AppInfoPage(),
            'webview':(BuildContext context) => WebViewFlutter(),
            'inbox':(BuildContext context) => InboxPage(),
          },
          debugShowCheckedModeBanner: false,

        )
    );

  }

  Route _createRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => MyHomePage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }


}