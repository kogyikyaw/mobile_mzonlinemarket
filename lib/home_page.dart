import 'dart:io';

import 'package:badges/badges.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_webview_project/app_info_page.dart';
import 'package:flutter_webview_project/utils/all_translations.dart';
import 'package:flutter_webview_project/utils/count_notifier.dart';
import 'package:flutter_webview_project/utils/sqlite_db_helper.dart';
import 'package:flutter_webview_project/utils/version_info.dart';
import 'package:flutter_webview_project/webview_page.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:store_redirect/store_redirect.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:uuid/uuid.dart';
import 'package:uuid/uuid_util.dart';

import 'inbox_page.dart';
import 'app_config.dart';
import 'utils/message_model.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {

  static FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _initialized = false;
  var appConfig;
  var notifier;
  var connectionNotifier;
  static const APP_STORE_URL =
      'https://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftwareUpdate?id=YOUR-APP-ID&mt=8';
  static const PLAY_STORE_URL =
      'https://play.google.com/store/apps/details?id=mz.online.market';
  final Color svgColor = Color(0xffed1a3b);
  final Color bgColor = Color(0xfff5f5f5);
  final String assetName = 'assets/images/MZOnlineMarket_logo.svg';



  @override
  Widget build(BuildContext context) {
    appConfig = AppConfig.of(context);
    notifier = Provider.of<CountNotifier>(context);
    final Widget svgIcon = SvgPicture.asset(
      assetName,
      //color: svgColor,
      height: 32.0,
      matchTextDirection: true,
      allowDrawingOutsideViewBox: true,
    );

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: bgColor,
        appBar: AppBar(
          centerTitle: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              svgIcon,
            ],
          ),
          actions: [
            Badge(
              showBadge: notifier.count != null && notifier.count > 0,
              position: BadgePosition.topStart(top: 2, start: 8),
              badgeColor: svgColor,
              shape: BadgeShape.circle,
              borderRadius: BorderRadius.circular(5),
              badgeContent: notifier.count != null && notifier.count > 0 ? Text(
                  notifier.count.toString()) : null,
              child: GestureDetector(
                child: Icon(Icons.notifications, color: svgColor,),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => InboxPage()),
                  );
                },
              ),
            ),
            IconButton(
              icon: Icon(Icons.subject, color: svgColor,),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AppInfoPage()),
                );
              },
            ),


          ],
        ),
        body: Stack(children: [
              Visibility(
                visible: Provider.of<DataConnectionStatus>(context) ==
                    DataConnectionStatus.disconnected,
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.signal_cellular_connected_no_internet_4_bar,
                          color: svgColor,),
                        onPressed: () {},
                      ),
                      Text(allTranslations.text("pls_check_connection"),
                        style: TextStyle(color: Colors.black45),),
                    ],
                  ),
                ),

              ),
              Visibility(
                  visible: Provider.of<DataConnectionStatus>(context) ==
                      DataConnectionStatus.connected,
                    child: _callWebView()
              ),

        ])
          //child: Text("hello", style: TextStyle(color: Colors.black12),),
        );
  }

  Widget _callWebView() {
    return WebViewFlutter(
      websiteName: appConfig.websiteName,
      websiteUrl: appConfig.websiteUrl,
    );
  }

  @override
  void initState() {
    _initLocalNotifications();
    _initMessaging();
    _getUnreadMessageAndNotify();
    _versionCheck(context);
  }

  Future<void> _initMessaging() async {
    if (!_initialized) {
      // For iOS request permission first.
      _firebaseMessaging.requestNotificationPermissions(
          const IosNotificationSettings(sound: true, badge: true, alert: true));

      _firebaseMessaging.configure(
        onMessage: (
            Map<String, dynamic> message) async { //call when app is foreground
          print("AppPushs onMessage: $message");
          convertFormattedMessageAndSave(message);
          _getUnreadMessageAndNotify();
          _showLocalNotification(message);
          return;
        },
        onBackgroundMessage: Platform.isIOS ? null : myBackgroundMessageHandler,
        //not known it will be called
        onLaunch: (Map<String,
            dynamic> message) async { //call when app is onDestroy if use click_action
          print("AppPushs onLaunch: $message");
          convertFormattedMessageAndSave(message);
          _getUnreadMessageAndNotify();
        },
        onResume: (Map<String,
            dynamic> message) async { //call when app is onPause and resume _onTapNotification if use click_action
          print('AppPushs onResume : $message');
          convertFormattedMessageAndSave(message);
          _getUnreadMessageAndNotify();
          _redirectToInboxPage();
          return;
        },
      );

      // For testing purposes print the Firebase Messaging token
      String token = await _firebaseMessaging.getToken();
      print("FirebaseMessaging token: $token");

      _initialized = true;
    }
  }

  static Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) {
    print('AppPushs myBackgroundMessageHandler');

    if (message.containsKey('notification')) {
      final dynamic notification = message['notification'];
      print('title' + notification['title']);
      print('title' + notification['body']);
    }
    if (message.containsKey('data')) {
      final dynamic data = message['data'];
      print('title' + data['title']);
      print('title' + data['description']);
      //handleNotification(message);
    }
    return Future<void>.value();
  }

  _initLocalNotifications() {
    var initializationSettingsAndroid = new AndroidInitializationSettings(
        '@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    _flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _onTapNotification
    );
  }

  //call when tap on local noti
  Future _onTapNotification(String payload) async {
    /*FirebaseMessageModel notiMessage = FirebaseMessageModel.fromRawJson(payload);
    _addToDb(notiMessage);
    _getUnreadMessageAndNotify();*/
    _redirectToInboxPage();
  }

  static Future _showLocalNotification(Map<String, dynamic> message) async {
    var pushTitle;
    var pushText;
    var action;
    var title;
    var desc;

    pushTitle = message['notification']['title'];
    pushText = message['notification']['body'];

    if (Platform.isAndroid) {
      var nodeData = message['data'];
      title = nodeData['title'];
      desc = nodeData['description'];
      action = nodeData['action'];
    } else {
      title = message['title'];
      desc = message['desc'];
      action = message['action'];
    }
    print("AppPushs params pushTitle : $pushTitle");
    print("AppPushs params pushText : $pushText");
    print("AppPushs params pushAction : $action");
    print("AppPushs params title : $title");
    print("AppPushs params desc : $desc");

    // @formatter:off
    var platformChannelSpecificsAndroid = new AndroidNotificationDetails(
        '00001',
        'test',
        'test channel',
        playSound: true,
        enableVibration: true,
        importance: Importance.Max,
        priority: Priority.High);
    // @formatter:on
    var platformChannelSpecificsIos = new IOSNotificationDetails(
        presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        platformChannelSpecificsAndroid, platformChannelSpecificsIos);
    var uuid = Uuid();
    var v4 = uuid.v4(); // -> '110ec58a-a0f2-4ac4-8393-c866d813b8d1'
    // Generate a v4 (crypto-random) id
    var v4_crypto = uuid.v4(options: {'rng': UuidUtil.cryptoRNG});

    print("uuidv4:  $v4_crypto");
    FirebaseMessageModel noti = FirebaseMessageModel(
        uuid: v4_crypto, title: title, description: desc, is_read: 0);
    String notiJsonString = noti.toRawJson();

    new Future.delayed(Duration.zero, () {
      _flutterLocalNotificationsPlugin.show(
        0,
        pushTitle,
        pushText,
        platformChannelSpecifics,
        payload: notiJsonString,
      );
    });
  }

  void _addToDb(FirebaseMessageModel message) async {
    print("uuid: " + message.uuid);
    await DatabaseHelper.instance.insert(message);
  }

  void convertFormattedMessageAndSave(Map<String, dynamic> message) {
    var uuid = Uuid();
    // Generate a v4 (crypto-random) id
    var v4_crypto = uuid.v4(options: {'rng': UuidUtil.cryptoRNG});

    print("uuidv4:  $v4_crypto");

    FirebaseMessageModel notiMessage = FirebaseMessageModel(uuid: v4_crypto,
        title: message['data']['title'],
        description: message['data']['description'],
        is_read: 0);
    _addToDb(notiMessage);
  }

  _getUnreadMessageAndNotify() {
    DatabaseHelper.instance.getUnreadCount().then((value) {
      setState(() {
        notifier.setCount(value);
      });
    }).catchError((error) {
      print(error);
    });
  }

  _redirectToInboxPage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => InboxPage()),
    );
  }


   _versionCheck(context) async {
    //Get Current installed version of app
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String currentVersionString = packageInfo.version;
    double currentVersion = double.parse(
        currentVersionString.trim().replaceAll(".", ""));

    //Get Latest version info from firebase config
    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    try {
      // Using default duration to force fetching from remote server.
      await remoteConfig.fetch(expiration: const Duration(seconds: 0));
      await remoteConfig.activateFetched();
      String remotejson = remoteConfig.getString(
          'force_update_current_version');
      VersionInfo versionInfo = VersionInfo.fromRawJson(remotejson);
      double newVersion = double.parse(versionInfo.version.trim()
          .replaceAll(".", ""));
      List<String> deprecated_versions = versionInfo.deprecated_versions;

      if (newVersion > currentVersion) {
        bool showLater = deprecated_versions.contains(currentVersionString)? true: false;
        _showVersionDialog(context, showLater);
      }

    } on FetchThrottledException catch (exception) {
      // Fetch throttled.
      print(exception);
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }
  }

  _showVersionDialog(context, bool showLater) async {
    await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return  WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
                backgroundColor: Colors.white,
                title: Text(allTranslations.text("force_update_title"), style: TextStyle(color: Colors.black)),
                content: Text(allTranslations.text("force_update_info"), style: TextStyle(color: Colors.black)),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () => _launchURL(Platform.isAndroid? PLAY_STORE_URL : APP_STORE_URL),
                    child: Text(allTranslations.text("update_now"), style: TextStyle(color: Colors.black)),
                  ),

                  Visibility(
                    visible: showLater,
                  child:FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(allTranslations.text("update_later"), style: TextStyle(color: Colors.black)),
                  ),
                  )

                ],
            )
        );
      },
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


}
