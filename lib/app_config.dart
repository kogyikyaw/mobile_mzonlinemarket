import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class AppConfig extends InheritedWidget {
  AppConfig({
    @required this.websiteName,
    @required this.flavorName,
    @required this.isDebugMode,
    @required this.osName,
    @required this.websiteUrl,
    @required this.debugShowCheckedModeBanner,
    @required Widget child,
  }) : super(child: child);

  final String websiteName;
  final String flavorName;
  final bool isDebugMode;
  final String osName;
  final String websiteUrl;
  final bool debugShowCheckedModeBanner;

  static AppConfig of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType(aspect: AppConfig);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
